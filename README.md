# README #

Welcome to the Gobierno de Aragón and related logos repository

### What is this repository for? ###

* This repository is intended to download the logo files to be used in Gobierno de Aragon apps.
* Logos for Gobierno de Aragón (DGA).
* Logos for FEDER Fondo Europeo de Desarrollo Regional.
* Logos for SDA Servicios Digitales de Aragón.
* Current version: v1.0

### How do I get set up? ###

* Just Download the desired AdobeXD file and export the assets.
* Or download directly the img files from the exported folders.

### Who do I talk to? ###

* Contact the SDA Servicios Digitales de Aragón team: https://sda.aragon.es/

### Software license

* Unless stated otherwise, the codebase is released under the https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12. This covers both the codebase and any sample code in the documentation.